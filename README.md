# React Circular Menu

A React Circular menu built in D3.js:

![](http://www.unshift.xyz/_subs/demo/github/react-circular-menu/assets/devices.png "Logo Title Text 1")

## Main Features

* responsive or fixed width
* smooth animations
* data-driven
* can display labels or icons (currently Awesome Fonts only)
* can work as CIRCLE, SEMI-CIRCLE and ARC
* customizable: styles, rotations, colors, sizes, spaces
* events: click, rollover, rollout
* works on desktop, web and mobile applications

[VIDEO]

#### Basic Usage:

```javascript
<CircularMenu data={this.state.data}
                      config={this.state.config}
                      onItemClick={(d) => this.onItemClick(d)} />
```

#### Complete Example

```javascript
import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
import * as PieTypes from 'xyz-components/constants/pie.types';

export default class Demo6 extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], config: {} };
  }

  componentDidMount() {
    this.init();
  }

  onItemClick(d) {
    window.open(d.link);
  }

  init() {
    this.setState({
      data: [
        { label: 'linkedin', icon: '\uf0e1', value: 20, bg: '#E0E0E0', color: '#222', link: 'https://www.abc.com'},
        ...
      ],
      config: {
        type: PieTypes.CIRCLE,
        width: null,
        showIcon: true,
        pieSize: 80,
      },
    });
  }

  render() {
    return (
      <div>
        <CircularMenu data={this.state.data}
                      config={this.state.config}
                      onItemClick={(d) => this.onItemClick(d)} />
      </div>
    );
  }
}

```


### Properties

|property|type|description|notes|
|---|---|---|---|
|data|array|the data provider of the menu|see example below|
|config|object|menu configuration|see example below|
|style|object|customize style for the wrapper component|-|
|selectedValue|string/number|the value of the element to highlight|-|


### callbacks
|property|type|description|
|---|---|---|
|onItemClick|function|invoked when user clicks a segment|
|onItemMouseOver|function|invoked when mouse is over a segment|
|onItemMouseOut|function|invoked when mouse leaves a segment|
|onTransitionEnd|function|invoked when the animation is completed|


### `data` array

```javascript
{
  label: 'linkedin',
  icon: '\uf0e1', // [awesome icon](https://fortawesome.github.io/Font-Awesome/cheatsheet/)
  value: 20,      // the weight/size of the element (the sum of all segments must be 100)
  bg: '#E0E0E0',  // the background color of the segment
  color: '#222',  // the color of the font / icon
  [ANY]: ...      // it will returned on click, mouseover and mouseout events
}

```
### `config` object

|property|type|description|
|---|---|---|
|width|number|the width of the menu. Set null to be responsive - default: null|
|backgroundColor|string|SVG background color (hexa, rgb, rgba - default: transparent)|
|type|object|one of these constants. (PieTypes.ARC, PieTypes.HALF, PieTypes.CIRCLE)
|showIcons|boolean|display icons (true) or labels (false). Currently only support [FontAwesome unicode icons](https://fortawesome.github.io/Font-Awesome/cheatsheet/)|
|iconRotation|boolean|labels follow the direction of the segment (default: false)|
|showCenteredLabel|boolean|display the centered label at rollover|
|centeredLabelClass|string|Custom class for the label centered. It appears when user rollover a segment|
|iconLabelCustomClass|string|Custom class for the text ( or the icon) into the segment|
|sizeIcon|string|icon font size. I.e. '2em'|
|pieSize|string|the size of the pie segment. I.e. '50px'|


---

## DEPENDENCIES

* [React](https://facebook.github.io/react/)
* [D3.js](http://d3js.org/)

---

## STYLING

Although if you can customize the look&feel of the component using the `configuration` and the `data` objects, you need to include some basic styles.
An example of these styles is available in the `demo` folder (`circular-menu.css`) and you can easily customize the styles of the each element:

```css
/* The label that appears when user rollover an element (default class )*/
.circular_menu_centered_label {
  fill: #111 !important;
  font-size: 20px !important;
  padding: 0;
  margin: 0;
}

/* Styles for Segment */
.menu-circular-segment {
  /* ... */
}

/* Styles for the selected menu item: TEXT/ICONS styles  */
.menu-circular-segment {
  font-size: 20px;

  /* Font color: texts of each segment can be manually
     set from the 'data' object but you can set a global color as shown below" */
  /* fill: red !important; */
}

/* Styles for the selected menu item: TEXT/ICONS styles  */
text.menu-circular-segment-selected {
  fill: orange !important;
  font-weight: 400;
  font-size: 1.5 em;
  pointer-events: none;
  text-decoration: underline;
}

/* Styles for the selected menu item: SEGMENT styles  */
path.menu-circular-segment-selected {
  fill: #333;
  /*opacity: 0.3 !important;*/
}
```

The following libraries are also useful. You can include them via CDN, Bower, npm, or any other way:

* [Font Awesome](https://fortawesome.github.io/Font-Awesome/): if you need to display icons
* [React Bootstrap](https://react-bootstrap.github.io/) is not required (but it's a good way to use circular menu as popover or modal)

```html
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./css/demo.css" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="./css/circular-menu.css" rel='stylesheet' type='text/css'>
```


---

## CLI: run, build, test, lint

### Build component
1. `npm install`
2. `npm start`

### Run demo

1. Go to `./demo` folder
2. `npm install`
3. `npm start`
4. browse http://localhost:3000

### Build the demo (distribution)

1. Go to `./demo` folder
2. `npm install`
3. `npm run start:umd`
4. browse http://localhost:8080/index-dist.html

### Run Test
This components uses [AVA](https://github.com/sindresorhus/ava), [Airbnb Enzyne](https://github.com/airbnb/enzyme), [JSDOM](https://github.com/tmpvar/jsdom), react-addons-test-utils and many other tools (see `package.json`).

1. Go the project root folder
2. `npm install`
3. `npm run test`

### Lint
1. Go the project root folder
2. `npm run lint`

---

# Authors and Contributors

* [Fabio Biondi](http://www.fabiobiondi.com) - Author
* [Matteo Ronchi](https://github.com/cef62/) - Contributor
* [Unshift](http://www.unshift.com)

Based on [library-boilerplate](https://github.com/gaearon/library-boilerplate) by Dan Abramov
