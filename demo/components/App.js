import React, { Component, PropTypes } from 'react';
import Demo1 from './Demo1';
import Demo2 from './Demo2';
import Demo3 from './Demo3';
import Demo4 from './Demo4';
import Demo5 from './Demo5';
import Demo6 from './Demo6';
import DemoPopover from './DemoPopover';
import DemoPopoverDuration from './DemoPopoverDuration';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = { tpl: this.props.list[0].component, index: 0 };
  }

  /**
   * On Select => Change the component
   */
  onSelect(event) {
    let tpl = '';
    if (event.target.value !== '') {
      tpl = this.props.list[event.target.value].component;
    }
    this.setState({ tpl, index: event.target.value });
  }

  render() {
    const { list } = this.props;

    // Generate Cell TEmplate
    const optionTpl = list.map(function getOptionItem(item, index) {
      return (
         <option value={index} key={'option' + index}>{item.text}</option>
      );
    });

    return (
      <div>
        <div className="select_style" >
          <select onChange={(event) => this.onSelect(event)} value={this.state.index} style={{ width: 300 }}>
            <option value="">Select</option>
            {optionTpl}
          </select>
        </div>
        <br /><br />

        <div style={{ textAlign: 'center' }}>
          {this.state.tpl}
        </div>

      </div>
    );
  }
}

App.defaultProps = {
  list: [
    { text: 'Circular Menu Showreel', component: <Demo1 /> },
    { text: 'Demo Time Picker', component: <DemoPopoverDuration /> },
    { text: 'Interactive Menu with subitems', component: <Demo5 /> },
    { text: 'ARC: responsive', component: <Demo2 /> },
    { text: 'Demo Color Picker', component: <DemoPopover /> },
    { text: 'Use as TabBar / Fixed footer', component: <Demo3 /> },
    { text: 'Sub-Menu', component: <Demo4 /> },
    { text: 'Basic Usage', component: <Demo6 /> },
    { text: 'Multiple Instances', component: <div><h1>Multiple Instances in the same page</h1><Demo4 /><Demo1 /></div> },
  ],
};

App.propTypes = {
  list: PropTypes.array,
};
