import React, { Component } from 'react';
// import Demo1 from './Demo1';
// import Demo2 from './Demo2';
// import Demo3 from './Demo3';
import Demo4 from './Demo4';

export default class App extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    return (
      <div>
        <div style={{width: '50%'}}>
        </div>
        <div style={{width: '50%'}}>
          {/* <Demo2 /> */}
          {/* <Demo1 /> */}
          <Demo4 />
          <h3>Built in React and D3.js</h3>
        </div>

        <div style={{position: 'absolute', width: '100%', bottom: 0}}>
          {/* <Demo3 /> */}
        </div>

      </div>
    );
  }
}
