import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
import { CIRCLE } from 'xyz-components/constants/pie.types';
import * as PieTypes from 'xyz-components/constants/pie.types';

const menus = [

  {
    data: [
      { label: 'contrast', icon: '\uf042', value: 20, bg: '#E0E0E0', color: '#222' },
      { label: 'battery', icon: '\uf244', value: 20, bg: '#BDBDBD', color: '#222' },
      { label: 'bluetooth', icon: '\uf293', value: 20, bg: '#9E9E9E', color: '#222' },
      { label: 'light', icon: '\uf0eb', value: 20, bg: '#757575', color: 'white' },
      { label: 'settings', icon: '\uf085', value: 20, bg: '#616161', color: 'white' },
    ],
    config: {
      type: PieTypes.HALF,
      colors: [],
      width: null,
      showIcon: true,
      pieSize: 80,
    },
  },
  {
    data: [
      { label: 'contrast', icon: '\uf042', value: 20, bg: 'rgb(158, 35, 88)', color: 'white' },
      { label: 'battery', icon: '\uf244', value: 20, bg: 'rgb(189, 53, 111)', color: 'white' },
      { label: 'bluetooth', icon: '\uf293', value: 20, bg: 'rgb(195, 62, 120)', color: 'white' },
      { label: 'light', icon: '\uf0eb', value: 20, bg: 'rgb(210, 77, 134)', color: 'white' },
      { label: 'settings', icon: '\uf085', value: 20, bg: 'rgb(217, 120, 162)', color: 'white' },
    ],
    config: {
      type: PieTypes.CIRCLE,
      colors: [],
      width: null,
      showIcon: true,
      pieSize: 40,
    },
  },
  {
    data: [
      { label: 'save', icon: '\uf0c7', value: 30, bg: '#F0F4C3' },
      { label: 'edit', icon: '\uf044', value: 40, bg: '#E6EE9C' },
      { label: 'send', icon: '\uf1d8', value: 30, bg: '#DCE775' },
    ],
    config: {
      type: PieTypes.CIRCLE,
      colors: [],
      width: null,
      showIcon: false,
      pieSize: 100,
    },
  },
  {
    data: [
      { label: 'save', icon: '\uf0c7', value: 30, color: 'white' },
      { label: 'edit', icon: '\uf044', value: 40, color: 'white' },
      { label: 'send', icon: '\uf1d8', value: 30, color: 'white' },
    ],
    config: {
      type: PieTypes.HALF,
      colors: [],
      width: null,
      showIcon: true,
      pieSize: 70,
    },
  },
];

export default class Demo1 extends Component {
  constructor(props) {
    super(props);
    this.state = {data: [], config: {}};
  }

  componentDidMount() {
    this.init();
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  /**
   * Click on segement / item
   */
  onItemClick(d) {
    console.log('SELECTED:', d);
  }

  init() {
    this.counter = 0;
    this.updateMenu();
    this.next();
  }

  next() {
    clearInterval(this.timer);
    this.timer = setTimeout(this.updateMenu.bind(this), 2000);
  }

  updateMenu() {
    const menuItem = menus[this.counter];
    this.setState({data: menuItem.data, config: menuItem.config});

    if (this.counter < menus.length - 1) {
      this.counter++;
    } else {
      this.counter = 0;
    }
    this.next();
  }

  render() {
    return (
      <div style={{width: '50%', maxWidth: 550, minWidth: 350, textAlign: 'center', margin: 'auto'}}>
        <h1>Circular Menu for React </h1>
        <h5>Choose another demo from the top-left select</h5>
        <CircularMenu type={CIRCLE} data={this.state.data} config={this.state.config} onItemClick={(d) => this.onItemClick(d)}/>

        <h3>MAIN FEATURES:</h3>
        <div>
          <li>responsive or fixed width</li>
          <li>smooth animations</li>
          <li>data-driven</li>
          <li>can display labels or icons (currently Awesome Fonts only)</li>
          <li>can work as CIRCLE, SEMI-CIRCLE and ARC</li>
          <li>customizable: styles, rotations, colors, sizes, spaces</li>
          <li>events: click, rollover, rollout</li>
          <li>works on desktop, web and mobile applications</li>
        </div>
        <h5>Built in Javascript (ES2015), D3.js and React</h5>
      </div>
    );
  }
}
