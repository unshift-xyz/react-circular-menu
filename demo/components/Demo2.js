import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
import { CIRCLE } from 'xyz-components/constants/pie.types';
import * as PieTypes from 'xyz-components/constants/pie.types';
import { menu } from './styles.js';

const menus = [
  {
    data: [
      { label: 'bluetooth', icon: '\uf293', value: 20, bg: '#827717', color: '#ffffff' },
      { label: 'light', icon: '\uf0eb', value: 20, bg: '#9E9D24', color: '#111' },
      { label: 'settings', icon: '\uf085', value: 20, bg: '#AFB42B', color: '#111' },
    ],
    config: {
      type: PieTypes.ARC,
      colors: [],
      width: null,
      showIcon: true,
      sizeIcon: '1em',
      pieSize: 26,
      centeredLabelFontSize: '3em',
    },
  },
  {
    data: [
      { label: 'save', icon: '\uf0c7', value: 30, color: 'white' },
      { label: 'edit', icon: '\uf044', value: 40, color: 'white' },
      { label: 'send', icon: '\uf1d8', value: 30, color: 'white' },
    ],
    config: {
      type: PieTypes.ARC,
      colors: [],
      width: null,
      showIcon: true,
      sizeIcon: '2em',
      pieSize: 100,
      centeredLabelFontSize: '3em',
    },
  },
  {
    data: [
      { label: 'contrast', icon: '\uf042', value: 20, bg: '#9575CD', color: 'white' },
      { label: 'battery', icon: '\uf244', value: 20, bg: '#7E57C2', color: 'white' },
      { label: 'bluetooth', icon: '\uf293', value: 20, bg: '#673AB7', color: 'white' },
      { label: 'light', icon: '\uf0eb', value: 20, bg: '#5E35B1', color: 'white' },
      { label: 'settings', icon: '\uf085', value: 20, bg: '#512DA8', color: 'white' },
    ],
    config: {
      type: PieTypes.ARC,
      colors: [],
      width: null,
      showIcon: true,
      sizeIcon: '2em',
      pieSize: 150,
      centeredLabelFontSize: '3em',
    },
  },
  {
    data: [
      { label: 'save', icon: '\uf0c7', value: 30, bg: '#43A047', color: 'white' },
      { label: 'edit', icon: '\uf044', value: 40, bg: '#388E3C', color: 'white' },
      { label: 'send', icon: '\uf1d8', value: 30, bg: '#2E7D32', color: 'white' },
    ],
    config: {
      type: PieTypes.ARC,
      colors: [],
      width: null,
      showIcon: false,
      sizeIcon: '1em',
      pieSize: 70,
      centeredLabelFontSize: '3em',
    },
  },
  {
    data: [
      { label: 'contrast', icon: '\uf042', value: 20, bg: '#DCEDC8', color: '#33691E' },
      { label: 'battery', icon: '\uf244', value: 20, bg: '#C5E1A5', color: '#33691E' },
      { label: 'bluetooth', icon: '\uf293', value: 20, bg: '#AED581', color: '#33691Es' },
      { label: 'light', icon: '\uf0eb', value: 20, bg: '#9CCC65', color: '#33691Es' },
      { label: 'settings', icon: '\uf085', value: 20, bg: '#8BC34A', color: '#33691Es' },
    ],
    config: {
      type: PieTypes.ARC,
      colors: [],
      width: null,
      showIcon: true,
      sizeIcon: '2em',
      pieSize: 80,
      centeredLabelFontSize: '3em',
    },
  },
];

export default class Demo2 extends Component {
  constructor(props) {
    super(props);
    this.state = {data: [], config: {}};
  }

  componentDidMount() {
    this.init();
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  /**
   * Click on segement / item
   */
  onItemClick(d) {
    console.log('SELECTED:', d);
  }

  init() {
    this.counter = 0;
    this.updateMenu();
    this.next();
  }

  next() {
    clearInterval(this.timer);
    this.timer = setTimeout(this.updateMenu.bind(this), 2000);
  }

  updateMenu() {
    const menuItem = menus[this.counter];
    this.setState({data: menuItem.data, config: menuItem.config});

    if (this.counter < menus.length - 1) {
      this.counter++;
    } else {
      this.counter = 0;
    }
    this.next();
  }

  render() {
    return (
      <div style={menu.size_md}>
        <CircularMenu type={CIRCLE} data={this.state.data} config={this.state.config} onItemClick={(d) => this.onItemClick(d)}/>
      </div>
    );
  }
}
