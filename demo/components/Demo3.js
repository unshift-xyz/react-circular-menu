import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
import { CIRCLE } from 'xyz-components/constants/pie.types';
import * as PieTypes from 'xyz-components/constants/pie.types';
import { menu } from './styles.js';

const menus = [
  {
    data: [
      { label: 'save', icon: '\uf0c7', value: 30, color: 'white' },
      { label: 'edit', icon: '\uf044', value: 40, color: 'white' },
      { label: 'send', icon: '\uf1d8', value: 30, color: 'white' },
    ],
    config: {
      type: PieTypes.HALF,
      colors: [],
      width: null,
      showIcon: true,
      pieSize: 90,
    },
  },
  {
    data: [
      { label: 'contrast', icon: '\uf042', value: 20, bg: '#9575CD', color: 'white' },
      { label: 'battery', icon: '\uf244', value: 20, bg: '#7E57C2', color: 'white' },
      { label: 'bluetooth', icon: '\uf293', value: 20, bg: '#673AB7', color: 'white' },
      { label: 'light', icon: '\uf0eb', value: 20, bg: '#5E35B1', color: 'white' },
      { label: 'settings', icon: '\uf085', value: 20, bg: '#512DA8', color: 'white' },
    ],
    config: {
      type: PieTypes.HALF,
      colors: [],
      width: null,
      showIcon: true,
      pieSize: 40,
    },
  },
  {
    data: [
      { label: 'save', icon: '\uf0c7', value: 30, bg: '#43A047', color: 'white' },
      { label: 'edit', icon: '\uf044', value: 40, bg: '#388E3C', color: 'white' },
      { label: 'send', icon: '\uf1d8', value: 30, bg: '#2E7D32', color: 'white' },
    ],
    config: {
      type: PieTypes.HALF,
      colors: [],
      width: null,
      showIcon: false,
      pieSize: 70,
    },
  },
  {
    data: [
      { label: '#43A047', icon: '\uf0c7', value: 30, bg: '#43A047', color: 'white' },
      { label: '#388E3C', icon: '\uf044', value: 40, bg: '#388E3C', color: 'white' },
      { label: '#388e74', icon: '\uf044', value: 40, bg: '#388e74', color: 'white' },
      { label: '#2E7D32', icon: '\uf1d8', value: 30, bg: '#2E7D32', color: 'white' },
    ],
    config: {
      type: PieTypes.HALF,
      colors: [],
      width: null,
      showIcon: false,
      iconRotation: true,
      iconLabelCustomClass: 'menu-circular-segment-small',
      pieSize: 90,
    },
  },
];

export default class Demo2 extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], config: {} };
  }

  componentDidMount() {
    this.init();
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  /**
   * Click on segement / item
   */
  onItemClick(d) {
    console.log('SELECTED:', d); // eslint-disable-line
  }

  init() {
    this.counter = 0;
    this.updateMenu();
    this.next();
  }

  next() {
    clearInterval(this.timer);
    this.timer = setTimeout(this.updateMenu.bind(this), 2000);
  }

  updateMenu() {
    const menuItem = menus[this.counter];
    this.setState({ data: menuItem.data, config: menuItem.config });

    if (this.counter < menus.length - 1) {
      this.counter++;
    } else {
      this.counter = 0;
    }
    this.next();
  }

  render() {
    return (
      <div>
        <h2>Semicircle as TabBar / Footer</h2>
        <h5>Responsive</h5>
        <div style={menu.footerMenu}>
          <CircularMenu type={CIRCLE} data={this.state.data} config={this.state.config} onItemClick={(d) => this.onItemClick(d)}/>
        </div>
      </div>
    );
  }
}
