import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
import * as PieTypes from 'xyz-components/constants/pie.types';
import { menu } from './styles.js';

export default class Demo5 extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], menuWidth: 250, config: {} };
  }

  componentDidMount() {
    // this.onItemClick({type: null});
    this.handleClickListener = (event) => this.handleClick(event);
    window.addEventListener('click', this.handleClickListener);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.handleClickListener);
  }

  /**
   * Click on segement / item
   */
  onItemClick(d) {
    if (d.type === 'CONTRAST') {
      this.setState({
        data: [
          { label: '', icon: '\uf00d', value: 20, bg: 'white', color: '#512DA8', type: 'ROOT' },
          { label: 'contrast', icon: '\uf042', value: 20, bg: '#9575CD', color: 'white', type: 'ROOT' },
          { label: 'battery', icon: '\uf244', value: 20, bg: '#7E57C2', color: 'white', type: 'ROOT' },
          { label: 'bluetooth', icon: '\uf293', value: 20, bg: '#673AB7', color: 'white', type: 'ROOT' },
          { label: 'light', icon: '\uf0eb', value: 20, bg: '#5E35B1', color: 'white', type: 'ROOT' },
          { label: 'settings', icon: '\uf085', value: 20, bg: '#512DA8', color: 'white', type: 'ROOT' },
          { label: 'contrast', icon: '\uf1a9', value: 20, bg: '#9575CD', color: 'white', type: 'ROOT' },
          { label: 'View', icon: '\uf206', value: 20, bg: '#7E57C2', color: 'white', type: 'ROOT' },
          { label: 'bluetooth', icon: '\uf0c3', value: 20, bg: '#673AB7', color: 'white', type: 'ROOT' },
          { label: 'light', icon: '\uf0e7', value: 20, bg: '#5E35B1', color: 'white', type: 'ROOT' },
        ],
        config: {
          type: PieTypes.CIRCLE,
          colors: [],
          width: null,
          showIcon: true,
          pieSize: 70,

        },
      });
    } else if (d.type === 'LIGHTS') {
      this.setState({
        data: [
          { label: '', icon: '\uf00d', value: 20, bg: 'white', color: 'rgb(193, 186, 33)', type: 'ROOT' },
          { label: 'contrast', icon: '\uf042', value: 20, bg: 'rgb(238, 232, 85)', color: '#bdac18', type: 'ROOT' },
          { label: 'battery', icon: '\uf244', value: 20, bg: 'rgb(246, 242, 130)', color: '#bdac18', type: 'ROOT' },
          { label: 'bluetooth', icon: '\uf293', value: 20, bg: 'rgb(238, 232, 85)', color: '#bdac18', type: 'ROOT' },
        ],
        config: {
          type: PieTypes.CIRCLE,
          colors: [],
          width: null,
          showIcon: true,
          pieSize: 70,
        },
      });
    } else if (d.type === 'BATTERY') {
      this.setState({
        data: [
          { label: '', icon: '\uf00d', value: 20, bg: 'white', color: 'rgb(121, 121, 121)', type: 'ROOT' },
          { label: 'contrast', icon: '\uf052', value: 20, bg: 'rgb(179, 179, 179)', color: '#222', type: 'ROOT' },
          { label: 'battery', icon: '\uf04b', value: 20, bg: 'rgb(140, 140, 140)', color: '#222', type: 'ROOT' },
          { label: 'bluetooth', icon: '\uf28d', value: 20, bg: 'rgb(133, 133, 133)', color: '#222', type: 'ROOT' },
        ],
        config: {
          type: PieTypes.CIRCLE,
          colors: [],
          width: null,
          showIcon: true,
          pieSize: 70,
        },
      });
    } else if (d.type === 'SETTINGS') {
      this.setState({
        data: [
          { label: 'back', icon: '\uf00d', value: 20, bg: 'white', color: 'rgb(194, 140, 47)', type: 'ROOT' },
          { label: 'contrast', icon: '\uf042', value: 20, bg: 'rgb(221, 170, 57)', color: 'white', type: 'ROOT' },
          { label: 'battery', icon: '\uf244', value: 20, bg: 'rgb(238, 194, 95)', color: 'white', type: 'ROOT' },
          { label: 'bluetooth', icon: '\uf293', value: 20, bg: 'rgb(212, 153, 23)', color: 'white', type: 'ROOT' },
        ],
        config: {
          type: PieTypes.CIRCLE,
          colors: [],
          width: null,
          showIcon: false,
          pieSize: 100,
        },
      });
    } else if (d.type === 'BLUETOOTH') {
      this.setState({
        data: [
          { label: 'enable', value: 70, bg: 'rgb(57, 172, 221)', color: 'white', type: 'ROOT' },
          { label: 'back', value: 30, bg: 'white', color: 'rgb(57, 172, 221)', type: 'ROOT' },
        ],
        config: {
          type: PieTypes.CIRCLE,
          colors: [],
          width: this.state.menuWidth,
          showIcon: false,
          pieSize: 100,
        },
      });
    } else {
      this.setState({
        data: [
          { label: 'contrast', icon: '\uf042', value: 20, bg: '#9575CD', color: 'white', type: 'CONTRAST' },
          { label: 'battery', icon: '\uf04b', value: 20, bg: 'grey', color: 'white', type: 'BATTERY' },
          { label: 'bluetooth', icon: '\uf293', value: 20, bg: 'rgb(142, 186, 223)', color: 'white', type: 'BLUETOOTH' },
          { label: 'light', icon: '\uf0eb', value: 20, bg: 'rgb(242, 224, 131)', color: 'white', type: 'LIGHTS' },
          { label: 'settings', icon: '\uf244', value: 20, bg: 'rgb(201, 143, 110)', color: 'white', type: 'SETTINGS' },

        ],
        config: {
          type: PieTypes.CIRCLE,
          colors: [],
          width: null,
          showIcon: true,
          pieSize: 50,
          showCenteredLabel: true,
          centeredLabelClass: 'circular_menu_centered_label',
          // backgroundColor: 'blue',
        },
      });
    }
  }

  handleClick(event) {
    const isShown = this.state.show;
    if (this.state.show) {
      // hide
      this.setState({ data: [] });
      this.setState({ show: !isShown });
    } else {
      // show
      const halfMenu = this.state.menuWidth / 2;
      this.setState({ show: !isShown, x: event.clientX - halfMenu, y: event.clientY - halfMenu });
      this.onItemClick({ type: null });
    }
  }

  render() {
    const { show, x, y } = this.state;
    const styles = Object.assign({}, menu.fixedMenu);
    styles.left = x;
    styles.top = y;

    if (show) {
      Object.assign(styles, menu.fixedMenuOpen);
    }
    return (
      <div>
        <h1>Interactive and submenu</h1>
        <div style={styles}>
          <CircularMenu ref="menu" data={this.state.data} config={this.state.config} onItemClick={(d) => this.onItemClick(d)}/>
        </div>

        <div>
          <li>Click anywhere to show the meu</li>
          <li>Select a menu item to display its sub items</li>
          <li>Select any subitems to fire an action</li>
          <li>Click anywhere to hide the menu</li>
        </div>
      </div>
    );
  }
}
