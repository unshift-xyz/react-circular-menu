import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
// import { CIRCLE } from 'xyz-components/constants/pie.types';
import * as PieTypes from 'xyz-components/constants/pie.types';
// import { menu } from './styles.js';

export default class Demo6 extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], config: {} };
  }

  componentDidMount() {
    this.init();
  }

  onItemClick(d) {
    window.open(d.link);
  }

  init() {
    this.setState({
      data: [
        { label: 'linkedin', icon: '\uf0e1', value: 20, bg: '#E0E0E0', color: '#222', link: 'https://www.linkedin.com/in/fabiobiondi' },
        { label: 'google+', icon: '\uf1a0', value: 20, bg: '#BDBDBD', color: '#222', link: 'https://plus.google.com/+FabioBiondi/posts/p/pub'},
        { label: 'blog', icon: '\uf19a', value: 20, bg: '#9E9E9E', color: '#222', link: 'http://www.fabiobiondi.com/blog'},
        { label: 'youtube', icon: '\uf167', value: 20, bg: '#757575', color: 'white', link: 'https://www.youtube.com/user/BiondiFabio'},
        { label: 'sitoweb', icon: '\uf0c1', value: 20, bg: '#616161', color: 'white', link: 'http://fabiobiondi.com'},
      ],
      config: {
        type: PieTypes.CIRCLE,
        colors: [],
        width: null,
        showIcon: true,
        sizeIcon: '2em',
        pieSize: 80,
        showCenteredLabel: true,
      },
    });
  }

  render() {
    return (
      <div style={{width: '50%', maxWidth: 400, textAlign: 'center', margin: 'auto'}}>
        <h2>Social Links</h2>
        <CircularMenu data={this.state.data}
                      config={this.state.config}
                      onItemClick={(d) => this.onItemClick(d)} />
      </div>
    );
  }
}
