import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
// import Popover from 'xyz-components/components/Popover';
import { OverlayTrigger, Popover, Button } from 'react-bootstrap';
// import { CIRCLE } from 'xyz-components/constants/pie.types';
import * as PieTypes from 'xyz-components/constants/pie.types';
// import { menu } from './styles.js';


export default class DemoPopover extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], config: {}, color: 'white' };
  }

  componentDidMount() {
    this.initPicker();
  }

  onItemClick(d) {
    this.setState({ data: [], color: d.bg });
    this.refs.popover1.hide();
  }

  initPicker() {
    this.setState({
      data: [],
      config: {
        type: PieTypes.CIRCLE,
        colors: [],
        width: null,
        showIcon: false,
        iconLabelCustomClass: 'menu-circular-segment-small',
        iconRotation: true,
        pieSize: 80,
        showCenteredLabel: true,
      },
    });
  }

  openPopover() {
    this.setState({
      data: [
        { label: '#222222', icon: null, value: 20, bg: '#222222', color: 'white' },
        { label: '#8BC34A', icon: null, value: 20, bg: '#8BC34A', color: '#222' },
        { label: '#DCEDC8', icon: null, value: 20, bg: '#DCEDC8', color: '#222' },
        { label: '#c45e5e', icon: null, value: 20, bg: '#c45e5e', color: '#222' },
        { label: '#448AFF', icon: null, value: 20, bg: '#448AFF', color: '#222' },
        { label: '#727272', icon: null, value: 20, bg: '#727272', color: 'white' },
        { label: '#B6B6B6', icon: null, value: 20, bg: '#B6B6B6', color: '#222' },
        { label: '#689F38', icon: null, value: 20, bg: '#689F38', color: 'white' },
        { label: '#8BC34A', icon: null, value: 20, bg: '#8BC34A', color: '#222' },
        { label: '#DCEDC8', icon: null, value: 20, bg: '#DCEDC8', color: '#222' },
        { label: '#c45e5e', icon: null, value: 20, bg: '#c45e5e', color: '#222' },
        { label: '#448AFF', icon: null, value: 20, bg: '#448AFF', color: '#222' },
        { label: '#727272', icon: null, value: 20, bg: '#727272', color: 'white' },
        { label: '#B6B6B6', icon: null, value: 20, bg: '#B6B6B6', color: '#222' },
      ],
    });
  }

  render() {
    const popover = (
      <Popover title="Popover bottom" id="popover1">
        <CircularMenu data={this.state.data} config={this.state.config} onItemClick={(d) => this.onItemClick(d)} />
      </Popover>
    );
    return (
      <div style={{ width: '50%', maxWidth: 400, textAlign: 'center', margin: 'auto' }}>
        <h2>Color Picker</h2>

        <OverlayTrigger ref="popover1" overlay={ popover } trigger="click" rootClose placement="bottom" onEntered={() => this.openPopover()}>
          <Button bsStyle="default" style={{ backgroundColor: this.state.color }}>Open Picker</Button>
        </OverlayTrigger>

        <h2>This popover requires React Bootstrap</h2>

        <div>
        <li>Labels are rotated </li>
        <li>Button color is changed when a segment is selected </li>
        </div>
      </div>
    );
  }
}
