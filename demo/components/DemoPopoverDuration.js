import React, { Component } from 'react';
import CircularMenu from 'xyz-components/components/CircularMenu';
import { OverlayTrigger, Popover, Button } from 'react-bootstrap';
import * as PieTypes from 'xyz-components/constants/pie.types';

const DEFAULT_DURATION = {
  hour: { label: '10', value: 10 },
  mins: { label: '50', value: 50 },
  ampm: { label: 'am', value: 'am' },
};

export default class DemoPopoverDuration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      config: {},
      step: {
        nextPickerIndex: 0,
        selected: {},
      },
      duration: DEFAULT_DURATION,
    };
    this.tempDuration = DEFAULT_DURATION;
  }

  componentDidMount() {
    this.initPicker();
  }

  onExit() {
    // Reset Step
    this.setState({ step: { nextPickerIndex: 0 } });
    // Reset Picker Data
    this.tempDuration = { hour: {}, mins: {}, ampm: {} };
  }

  onTransitionEnd() {
    if (this.state.step.nextPickerIndex === 0) {
      // Close the picker
      this.refs.popover1.hide();
    }
  }

  pickerHourClass(index) {
    return (this.state.step.nextPickerIndex === index) ? 'popover_time_text_selected' : 'popover_time_text';
  }

  initPicker() {
    this.setState({
      data: [],
      config: {
        type: PieTypes.CIRCLE,
        // width: null,
        showIcon: false,
        iconRotation: false,
        pieSize: 80,
        showCenteredLabel: true,
        centeredLabelClass: 'circular_menu_centered_label_duration',
      },
    });
  }


  loadDataInPicker(d) {
    let pickerData = [];
    // Get the picker step
    const step = Object.assign({}, this.state.step);

    // Check the step index
    switch (step.nextPickerIndex) {
      // Last step of the picker
      case 3:
        pickerData = [];
        // step.nextPickerIndex = 0;
        this.tempDuration.ampm = d;
        // Duration is now the tempDuration (from picker)
        this.setState({ data: pickerData, duration: this.tempDuration, step: { nextPickerIndex: 0, selected: { } } });
        // Close the popup
        // this.refs.popover1.hide();
        break;

      // AM/PM Picker
      case 2:
        // Picker Data
        pickerData = [
          { label: 'PM', icon: null, value: 'pm', bg: 'rgb(91, 140, 249)', color: '#fff' },
          { label: 'AM', icon: null, value: 'am', bg: 'rgb(234, 237, 94)', color: '#222' },
        ];
        // Save previous selected value
        this.tempDuration.mins = d;
        // go to next step
        this.setState({ data: pickerData, step: { nextPickerIndex: 3, selected: this.state.duration.ampm } });
        break;

      // Minutes picker
      case 1:
        pickerData = [
          { label: '10', icon: null, value: 10, bg: 'rgb(162, 232, 137)', color: '#fff' },
          { label: '20', icon: null, value: 20, bg: 'rgb(162, 222, 137)', color: '#fff' },
          { label: '30', icon: null, value: 30, bg: 'rgb(162, 212, 137)', color: '#fff' },
          { label: '40', icon: null, value: 40, bg: 'rgb(162, 202, 137)', color: '#fff' },
          { label: '50', icon: null, value: 50, bg: 'rgb(162, 192, 137)', color: '#fff' },
        ];
        this.tempDuration.hour = d;
        this.setState({ data: pickerData, step: { nextPickerIndex: 2, selected: this.state.duration.mins } });
        break;

      // Hour Picker
      case 0:
      default:
        pickerData = [
          { label: '01', icon: null, value: 1, bg: '#ffffff', color: '#666' },
          { label: '02', icon: null, value: 2, bg: '#ffffee', color: '#666' },
          { label: '03', icon: null, value: 3, bg: '#ffffdd', color: '#666' },
          { label: '04', icon: null, value: 4, bg: '#ffffcc', color: '#666' },
          { label: '05', icon: null, value: 5, bg: '#ffffbb', color: '#666' },
          { label: '06', icon: null, value: 6, bg: '#ffffaa', color: '#666' },
          { label: '07', icon: null, value: 7, bg: '#ffff99', color: '#666' },
          { label: '08', icon: null, value: 8, bg: '#ffff88', color: '#666' },
          { label: '09', icon: null, value: 9, bg: '#ffff77', color: '#666' },
          { label: '10', icon: null, value: 10, bg: '#ffff66', color: '#666' },
          { label: '11', icon: null, value: 11, bg: '#ffff55', color: '#666' },
          { label: '12', icon: null, value: 12, bg: '#ffff44', color: '#666' },
        ];
        this.setState({ data: pickerData, step: { nextPickerIndex: 1, selected: this.state.duration.hour } });
        break;
    }
  }

  /**
   * Open the popover(when user clicks the button)
   */
  openPopover() {
    // Save current duration as picker temporary duration
    this.tempDuration = Object.assign({}, this.state.duration);
    // Load the first step of the picker
    this.loadDataInPicker();
  }

  render() {
    // const { duration, step: step = { selected: { label: {}, value: {}, ampm: {} } } } = this.state;
    const { duration, step: step = { selected: {} } } = this.state;
    const { selected: selected = {} } = step;

    // Popover creation
    const popover = (
        <Popover title="Popover bottom" id="popover1">
          <span className={ this.pickerHourClass(1)}>{ this.tempDuration.hour.label }:</span>
          <span className={ this.pickerHourClass(2)}>{ this.tempDuration.mins.label }</span>
          <span className={ this.pickerHourClass(3)}>{ this.tempDuration.ampm.label }</span>
          { /* Circular Menu */ }
          <CircularMenu data={this.state.data} config={this.state.config} selectedValue={ selected.value } onItemClick={(d) => this.loadDataInPicker(d)} onTransitionEnd={() => this.onTransitionEnd()}/>
        </Popover>
      );
    return (
      <div style={{ width: '50%', maxWidth: 400, textAlign: 'center', margin: 'auto' }}>
        <h2>Time Picker</h2>

        { /* React Bootstrap Overlay / Popover component è Trigger Button */ }
        <OverlayTrigger ref="popover1" overlay={ popover } trigger="click" rootClose placement="bottom" onEntered={() => this.openPopover()} onExit={() => this.onExit() }>
          <Button bsStyle="default">
            { duration.hour.label }:
            { duration.mins.label }
            { duration.ampm.label }
          </Button>
        </OverlayTrigger>

        <div>
          <li><strong>CircularMenu is not a Time Picker but you can easily built your own components</strong></li>
          <li>Three steps: hour, minutes, am/pm</li>
          <li>Each step has custom data and configuration</li>
          <li>When a new step is shown the menu highlights the previous choice</li>
        </div>
      </div>
    );
  }
}
