export const menu = {
  size_sm: {
    width: '100%',
    // width: 400,
    maxWidth: 300,
    textAlign: 'center',
    margin: 'auto',
  },
  size_md: {
    width: '100%',
    // width: 400,
    maxWidth: 500,
    textAlign: 'center',
  },
  fixedMenu: {
    position: 'absolute',
    // top: 200,
    // left: 200,
    // opacity: 0.2,
  },
  footerMenu: {
    position: 'fixed',
    bottom: 0,
    left: '10%',
    right: '10%',
    width: '80%',
    maxWidth: 600,
    textAlign: 'center',
    margin: 'auto',
  },
  fixedMenuOpen: {
    // opacity: 1,
  },
};
