/**
 * Circular Menu
 * Inspiration:
 * http://bl.ocks.org/j0hnsmith/5591116
 */
import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import d3 from 'd3';

import * as PieTypes from '../constants/pie.types';
import * as PieLayoutUtils from '../utils/pie.layout.utils.js';
import * as SVGUtils from '../utils/svg.utils.js';
import * as TextUtils from '../utils/pie.text.utils.js';

const DEFAULT_CONFIG = {
  type: PieTypes.HALF,
  colors: [],
  width: null,
  // innerRadius: 50,
  backgroundColor: 'rgba(255, 255, 255, 0)',    // white transparent
  showIcon: false,
  iconLabelCustomClass: 'menu-circular-segment',
  iconRotation: false,
  showCenteredLabel: true,
  centeredLabelClass: 'circular_menu_centered_label',
};

export default class CircularMenu extends Component {
    constructor(props) {
      super(props);
    }
  /**
   * INIT SVG elements and D3.js stuff
   */
  componentDidMount() {
    const { data, config } = this.props;
    const configuration = Object.assign({}, DEFAULT_CONFIG, this.props.config);
    // init svg, pie, centered text
    this.svgInstance = SVGUtils.instance(ReactDOM.findDOMNode(this), configuration);
    this.pie = PieLayoutUtils.instance(this.svgInstance);
    this.text = TextUtils.instance(this.svgInstance, configuration);
    // render pie if data
    if (data) {
      this.svgInstance.render(data, config);
    }
    // Namespaced resize event
    const ns = new Date().valueOf();
    d3.select(window).on('resize.' + ns, () => this.redraw(this.props));
  }

  /**
   * New props are arrived
   */
  componentWillReceiveProps(nextProps) {
    this.redraw(nextProps);
  }

  /**
   * Click on segment / item
   */
  onItemClick(d) {
    this.text.hide();
    this.props.onItemClick(d);
  }

  /**
   * Click on segment / item
   */
  onItemMouseOver(d) {
    const configuration = Object.assign({}, DEFAULT_CONFIG, this.props.config);
    if (configuration.showCenteredLabel) {
      this.text.render(d.label, configuration);
      this.props.onItemMouseOver(d);
    }
  }

  /**
   * Click on segment / item
   */
  onItemMouseOut(d) {
    this.text.hide();
    this.props.onItemMouseOut(d);
  }

  onTransitionEnd() {
    this.props.onTransitionEnd();
  }
  /**
   * Redraw elements
   */
  redraw(props) {
    const { data, config, selectedValue } = props;
    const configuration = Object.assign({}, DEFAULT_CONFIG, config);

    if (data) {
      // render svg
      this.svgRender = this.svgInstance.render(data, configuration);

      // render pie
      this.pieObject = this.pie.render(data,
                                       configuration,
                                       this.svgRender,
                                       (d) => this.onItemClick(d),
                                       (d) => this.onItemMouseOver(d),
                                       (d) => this.onItemMouseOut(d),
                                       (d) => this.onTransitionEnd(d)
                                      );
      this.pie.selectedItem(selectedValue);
    }
  }
  render() {
    return <div styles={this.props.style}></div>;
  }
}

/**
 * PropTypes validation
 */
CircularMenu.propTypes = {
  data: PropTypes.array,
  // the selected value
  selectedValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  config: PropTypes.shape(
    {
      // fixed width. If not set the menu is responsive
      width: PropTypes.number,
      // fixed height - still not supported
      height: PropTypes.number,
      // SVG background color (hexa, rgb, rgba - default: transparent)
      backgroundColor: PropTypes.string,
      // labels follow the direction of the segment (default: false)
      iconRotation: PropTypes.bool,
      // the type of menu, a constant: PieTypes.ARC, PieTypes.HALF, PieTypes.CIRCLE
      type: PropTypes.object,
      // true: display icons | false: display labels
      showIcon: PropTypes.bool,
      // show the centered label at rollover
      showCenteredLabel: PropTypes.bool,
      // Custom class for the label centered (appears when user rollover a segment)
      centeredLabelClass: PropTypes.string,
      // Custom class for the text ( or the icon) into the segment
      iconLabelCustomClass: PropTypes.string,
      // sizeIcon: PropTypes.string,
      // segment size (in px)
      pieSize: PropTypes.number,
      // colors: PropTypes.array,
      // innerRadius: PropTypes.number,
    }
  ),
  style: PropTypes.object,
  onItemClick: PropTypes.func,
  onItemMouseOver: PropTypes.func,
  onItemMouseOut: PropTypes.func,
  onTransitionEnd: PropTypes.func,
};


/**
 * Default properties
 */
CircularMenu.defaultProps = {
  data: null,
  style: {},
  selectedValue: null,
  onItemClick() {},
  onItemMouseOver() {},
  onItemMouseOut() {},
  onTransitionEnd() {},
};
