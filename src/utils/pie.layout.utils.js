import d3 from 'd3';
import { enterAntiClockwise } from '../constants/pie.types';

const bg = d3.scale.category20c();

// export function instance(svgInstance, data, config, itemClickCallBack) {
export function instance(_svgInstance) {
  const svgInstance = _svgInstance;
  const arc = d3.svg.arc();
  const arcText = d3.svg.arc();
  const pie = d3.layout.pie()
                .value(function pieValue(d) { return d.weight || 1; })
                .sort(null);
  let data = null;
  let config = null;
  let svgRender = null;
  let itemClickCallBack = null;
  let onItemMouseOverCallBack = null;
  let onItemMouseOutCallBack = null;
  let onTransitionEnd = null;

  // -------------------------------------------------------
  // Utilities
  // -------------------------------------------------------

  // Store the displayed angles in _current.
  // Then, interpolate from _current to the new angles.
  // During the transition, _current is updated in-place by d3.interpolate.
  function arcTween(a) {
    const i = d3.interpolate(this._current, a);
    this._current = i(0);
    return function arcFunc(t) {
      return arc(i(t));
    };
  }

  // Interpolate exiting arcs start and end angles to Math.PI * 2
  // so that they 'exit' at the end of the data
  function arcTweenOut() {
    const i = d3.interpolate(this._current, { startAngle: Math.PI * 2, endAngle: Math.PI * 2, weight: 0 });
    this._current = i(0);
    return function arcFunc(t) {
      return arc(i(t));
    };
  }

  // Tween used to animated the radius
  function tweenRadius(b) {
    return function arcFunc(a, i) {
      const d = b.call(this, a, i);
      const _i = d3.interpolate(a, d);
      for (const k in d) {
        if (a) {
          a[k] = d[k];
        }
      }
      return function func(t) { return arc(_i(t)); };
    };
  }

  // Check if all transitions are ended
  // Solution on SO: http://stackoverflow.com/questions/10692100/invoke-a-callback-at-the-end-of-a-transition
  function endall(transition, callback) {
    if (transition.size() === 0) { callback(); }
    let n = 0;
    transition
        .each(function increment() { ++n; })
        .each('end', function end() { if (!--n) callback.apply(this, arguments); });
  }

  // -------------------------------------------------------
  // DRAW functions
  // -------------------------------------------------------

  /**
   * Public method to seled an item of the ie
   */
  function selectedItem(value) {
    svgInstance.container.selectAll('path').data(pie(data))
      .classed('menu-circular-segment-selected', function classSelected(d) {
        return d.data.value === value;
      });

    svgInstance.container.selectAll('text').data(pie(data))
      .classed('menu-circular-segment-selected', function classSelected(d) {
        return d.data.value === value;
      });
  }

  /**
   * Render ARC segments
   */
  function renderSegments() {
    // Get width from last svg renderer
    const w = config.type.getWidth(svgRender.width);

    // TO REMOVE: the following is the previous way to get width container
    // const svgWidth = svgInstance.svg.style('width').replace('px', '');
    // const w = config.type.getWidth(svgWidth);

    // Update pie radius
    arcText.outerRadius(w).innerRadius(w - config.pieSize);
    // arc.outerRadius(w).innerRadius(w - config.pieSize);

    // path segments join
    const path = svgInstance.container.selectAll('path')
                    .data(pie(data));
                    // Disable mouse events duting transitions

    // Update inner radius
    path.transition()
     .duration(500)
     .attrTween('d', tweenRadius(function tween() {
       return {
         innerRadius: w,
         outerRadius: w - config.pieSize,
       };
     }));

    // enter selection
    path.enter().append('path')
      .classed('menu-circular-segment', true)
     .attr('d', arc(enterAntiClockwise))
     .attr('fill', 'white')
     .style('opacity', 0)
     // set the start and end angles to Math.PI * 2 so we can transition
     // anticlockwise to the actual values
     .each(function each(d) {
       this._current = {
         data: d.data,
         weight: d.weight || 1,
         startAngle: enterAntiClockwise.startAngle,
         endAngle: enterAntiClockwise.endAngle,
         outerRadius: w,
         innerRadius: w - config.pieSize,
       };
     })
     .on('click', function clickHandler(d) {
       d3.event.stopPropagation();
       d3.select(this).style('opacity', 0.7).style('cursor', 'pointer');
       itemClickCallBack(d.data);
     })
     .on('mouseover', function mouseOver(d) {
       d3.event.stopPropagation();
       d3.select(this).style('opacity', 0.7).style('cursor', 'pointer');
       onItemMouseOverCallBack(d.data);
     })
     .on('mouseout', function mouseOut(d) {
       d3.event.stopPropagation();
       d3.select(this).style('opacity', 1).style('cursor', 'auto');
       onItemMouseOutCallBack(d.data);
     });

    // Exit selection
    path.exit()
      .transition()
      .duration(750)
      .attrTween('d', arcTweenOut)
      .call(endall, onTransitionEnd)
      .remove();

    // Update selection
    path.transition().duration(750).attrTween('d', arcTween)
        .style('opacity', 1)
        // Disable mouse events during transition
        .style('pointer-events', 'none')
        .attr('fill', function fill(d, i) {
          const dt = data[i];
          return (dt.bg) ? dt.bg : bg(i);
        })
        .each('end', function eachEnd() {
          // Enable at end animation
          d3.select(this).style('pointer-events', 'auto');
        });
  }

  /**
   * Render Labels
   */

  // Label rotation follow the direction of the segment
  function angle(d) {
    const a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
    return a > 90 ? a - 180 : a;
  }

  // Render segment texts / icons
  function renderLabels() {
    // join
    const texts = svgInstance.container.selectAll('text')
     .data(pie(data));

    // enter selection
    texts.enter().append('text')
      .style('opacity', 0)
      .attr('text-anchor', function textAnchor() {
        return config.type.textAnchor;
      })
      .style('fill', function fill(d, i) {
        const dt = data[i];
        return (dt.color) ? dt.color : config.colorIcon;
      });

    // Update selection
    texts
      .style('pointer-events', 'none')
      .attr('font-family', 'FontAwesome')
      .attr('class', config.iconLabelCustomClass)
      .text(function text(d, i) {
        const dt = data[i];
        return (config.showIcon) ? dt.icon : dt.label;
      })
      .transition().duration(760)
      .delay(function delay(d, i) {
        return i * 100;
      })
      .attr('dy', function dy() {
        return this.getBBox().height / 2 - 2;
        // return config.type.textY;
      })
      .style('fill', function fill(d, i) {
        const dt = data[i];
        return (dt.color) ? dt.color : '#555';
      })
      .style('opacity', 1)
      .attr('transform', function transform(d) {
        let tsl = '';
        if (config.iconRotation) {
          // Labels follow the direction of the segments
          tsl = 'translate(' + arcText.centroid(d) + ')rotate(' + angle(d) + ')';
        } else {
          // Labels are no rotated
          tsl = 'translate(' + arcText.centroid(d) + ')';
        }
        return tsl;
      });

    // Exit selection
    texts.exit()
        .transition()
        .duration(750)
        .style('opacity', 0)
        .remove();
  }


  /**
   * Render function (when data changes or is resized)
   */
  function render(_data, _config, _svgRender, _itemClickCallBack, _onItemMouseOverCallBack, _onItemMouseOutCallBack, _onTransitionEnd) {
    data = _data;
    config = _config;
    svgRender = _svgRender;
    itemClickCallBack = _itemClickCallBack;
    onItemMouseOverCallBack = _onItemMouseOverCallBack;
    onItemMouseOutCallBack = _onItemMouseOutCallBack;
    onTransitionEnd = _onTransitionEnd;

    pie.value(function pieValue(d) { return d.weight || 1; })
       .sort(null)
       .startAngle(config.type.startAngle)
       .endAngle(config.type.endAngle);

    renderSegments();
    renderLabels();
  }

  // Public
  return {
    pie,
    arc,
    render,
    selectedItem,
  };
}
