export function instance(_svgInstance, configuration) {
  const svgInstance = _svgInstance;
  const text = _svgInstance.svg
    .append('text')
    .style('pointer-events', 'none')
    .attr('class', configuration.centeredLabelClass);

  function render(label, cfg) {
    text.text('');
    const svgWidth = svgInstance.svg.style('width').replace('px', '');
    text.text(label)
    .style('line-height', 'normal')
    // .classed('centerLabel', true)
    .attr('class', cfg.centeredLabelClass)
    // .attr('font-size', centeredLabelFontSize)
    .style('text-anchor', cfg.type.getTextCenteredAnchor)
    .attr('dx', function dx() {
      return cfg.type.getTextCenteredX(svgWidth, this.getBBox());
    })
    .attr('dy', function dy() {
      return cfg.type.getTextCenteredY(svgWidth, this.getBBox());
    })
    .style('opacity', 0)
    .transition()
    .style('opacity', 1);
  }

  function hide() {
    text.transition()
        .style('opacity', 0);
  }

  return {
    hide,
    render,
  };
}
