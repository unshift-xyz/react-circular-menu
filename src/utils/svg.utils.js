import d3 from 'd3';

export function instance(element, cfg) {
  let width;
  let height;

  // SVG Creation
  const el = d3.select(element);
  const svg = el.append('svg').style('background-color', cfg.backgroundColor);
  let container = svg.append('svg:g');

  // SVG Render
  function render(data, config) {
    const { type, backgroundColor } = config;

    if (config.width) {
      // fixed width
      width = config.width;
    } else {
      // responsive
      const parentNode = d3.select(element.parentElement);
      const parentWidth = parseInt(parentNode[0][0].offsetWidth, 10);
      width = parentWidth;
    }
    // background color
    svg.style('background-color', backgroundColor);

    // svg and container position in according with the menu TYPE
    if (type) {
      height = type.getHeight(width);
      container = el.select('svg')
        .data([data])
            .transition().duration(500)
            .attr('width', width)
            .attr('height', height)
              .select('g')
                .attr('transform', 'translate(' + type.getPos(width).x + ',' + type.getPos(width).y + ')');
    }

    return {
      width,
    };
  }

  // Public properties
  return {
    render,
    svg,
    width,
    container,
  };
}
