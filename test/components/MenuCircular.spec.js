/* TODO */
/*eslint-disable */
/* eslint no-console:0,react/no-multi-comp:0 */
import test from 'ava';
import React, { Component } from 'react';
import 'babel-core/register';
import initJsDom from '../helpers/_document';
import CircularMenu from '../../src/components/CircularMenu';
import * as PieTypes from '../../src/constants/pie.types';
// import sinon from 'sinon';
// import ReactDOM from 'react-dom';
// import TestUtils from 'react-addons-test-utils';
// import { shallow } from 'enzyme';
// import { describeWithDOM, mount, spyLifecycle } from 'enzyme';

initJsDom();

// // Helper component
class Container extends Component {
  render() {
    const data = [
        { label: 'linkedin', icon: '\uf0e1', value: 20, bg: '#E0E0E0', color: '#222', link: 'https://www.linkedin.com/in/fabiobiondi' },
        { label: 'google+', icon: '\uf1a0', value: 20, bg: '#BDBDBD', color: '#222', link: 'https://plus.google.com/+FabioBiondi/posts/p/pub'},
        { label: 'blog', icon: '\uf19a', value: 20, bg: '#9E9E9E', color: '#222', link: 'http://www.fabiobiondi.com/blog'},
        { label: 'youtube', icon: '\uf167', value: 20, bg: '#757575', color: 'white', link: 'https://www.youtube.com/user/BiondiFabio'},
        { label: 'sitoweb', icon: '\uf0c1', value: 20, bg: '#616161', color: 'white', link: 'http://fabiobiondi.com'},
    ];
    const config = {
      type: PieTypes.CIRCLE,
      width: null,
      pieSize: 80,
    };
    return (<CircularMenu ref="menu" data={data} config={config} />);
  }
}


// -------------------------------------------
// Check children
// -------------------------------------------

test.cb('Todo: working in progress', t => {
  const data = [
      { label: 'linkedin', icon: '\uf0e1', value: 20, bg: '#E0E0E0', color: '#222', link: 'https://www.linkedin.com/in/fabiobiondi' },
      { label: 'google+', icon: '\uf1a0', value: 20, bg: '#BDBDBD', color: '#222', link: 'https://plus.google.com/+FabioBiondi/posts/p/pub' },
      { label: 'blog', icon: '\uf19a', value: 20, bg: '#9E9E9E', color: '#222', link: 'http://www.fabiobiondi.com/blog' },
      { label: 'youtube', icon: '\uf167', value: 20, bg: '#757575', color: 'white', link: 'https://www.youtube.com/user/BiondiFabio' },
      { label: 'sitoweb', icon: '\uf0c1', value: 20, bg: '#616161', color: 'white', link: 'http://fabiobiondi.com' },
  ];
  const config = {
    type: PieTypes.CIRCLE,
    width: null,
    pieSize: 80,
  };
  // Mount the Container wrapper
  // const wrapper = mount(<CircularMenu ref="pippo" data={data} config={config} />);
  // const wrapper = mount(<Container />);
  // wrapper.setProps({ data, config });
  t.pass();
  t.end();
});
/*eslint-enable */
