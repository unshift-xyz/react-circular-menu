/* eslint no-console:0,react/no-multi-comp:0 */
import test from 'ava';
import 'babel-core/register';
import initJsDom from '../helpers/_document';
import * as SVGUtils from '../../src/utils/svg.utils';
import * as PieLayoutUtils from '../../src/utils/pie.layout.utils';
import * as PieTypes from '../../src/constants/pie.types';


initJsDom();

test.cb('Required objects are generated when initialized', t => {
  const config = {
    type: PieTypes.CIRCLE,
    width: null,
    pieSize: 80,
  };
  const el = document.createElement('DIV');
  const svgInstance = SVGUtils.instance(el, config);
  const pieInstance = PieLayoutUtils.instance(svgInstance);
  t.is(pieInstance.hasOwnProperty('pie'), true);
  t.is(pieInstance.hasOwnProperty('arc'), true);
  t.is(pieInstance.hasOwnProperty('selectedItem'), true);
  t.is(pieInstance.hasOwnProperty('render'), true);
  t.end();
});
