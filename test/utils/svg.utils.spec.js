/* eslint no-console:0,react/no-multi-comp:0 */
import test from 'ava';
import 'babel-core/register';
import initJsDom from '../helpers/_document';
import * as SVGUtils from '../../src/utils/svg.utils';
import * as PieTypes from '../../src/constants/pie.types';


initJsDom();

test.cb('SVG custom background color is applied', t => {
  const config = {
    type: PieTypes.CIRCLE,
    width: null,
    showIcon: true,
    pieSize: 80,
    showCenteredLabel: true,
    backgroundColor: 'red',
  };
  const el = document.createElement('DIV');
  const a = SVGUtils.instance(el, config);
  const child = a.svg[0][0];
  t.is(child.style.backgroundColor, 'red');
  t.end();
});


test.cb('Should create and add an SVG to the owner element', t => {
  const config = {
    type: PieTypes.CIRCLE,
    width: null,
    showIcon: true,
    pieSize: 80,
    showCenteredLabel: true,
  };
  const el = document.createElement('DIV');
  const a = SVGUtils.instance(el, config);
  t.is(a.svg[0][0].namespaceURI, 'http://www.w3.org/2000/svg');

  // Similar to previous test, it checks if there is an SVG in the provided element
  t.is(el.querySelectorAll('svg').length, 1);
  t.end();
});


test.cb('The SVG element contains a <group /> child', t => {
  const config = {
    type: PieTypes.CIRCLE,
    width: null,
    showIcon: true,
    pieSize: 80,
    showCenteredLabel: true,
  };
  const el = document.createElement('DIV');
  const a = SVGUtils.instance(el, config);
  const child = a.svg[0][0].children[0];
  t.is(child.localName, 'g');
  t.end();
});

// TODO:test the method `SVGUtils.instance.render(data, config)`
