'use strict';

var webpack = require('webpack');

var reactExternal = {
  root: 'React',
  commonjs2: 'react',
  commonjs: 'react',
  amd: 'react',
};

var reactDOMExternal = {
  root: 'ReactDOM',
  commonjs2: 'react-dom',
  commonjs: 'react-dom',
  amd: 'react-dom',
};

var d3External = {
  root: 'd3',
  commonjs2: 'd3',
  commonjs: 'd3',
  amd: 'd3',
};

var plugins = [
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
  }),
];

if (process.env.NODE_ENV === 'production') {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
    compressor: {
      screw_ie8: true,
      warnings: false,
    }}));
}

module.exports = {
  externals: {
    'react': reactExternal,
    'react-dom': reactDOMExternal,
    'd3': d3External,
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel-loader'],
      exclude: /node_modules/,
    }]
  },
  output: {
    library: 'react-circular-menu',
    libraryTarget: 'umd',
  },
  plugins: plugins,
  resolve: {
    extensions: ['', '.js'],
  }
};
